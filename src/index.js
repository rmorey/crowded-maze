import 'phaser';
const levenshtein = require('js-levenshtein');


var config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: 800,
    height: 800,
    physics: {
        default: 'arcade',
        arcade: {
            debug: false
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);
var cursors;
var player;
var walls;
var npcs;
var SPEED = 220;
var intToLetter = "ulrdabcd   ";
var THRESHOLD = 1; 
var yardstick = "";
const N_NPCS = 50;
const GENE_LEN = 500;


function genRandomGene(length) {
    var gene = "";
    for (var i = 0; i < length; i += 1) {
        gene += intToLetter[Math.floor(intToLetter.length*Math.random())];
    } 
    return gene;
}

function getNewPopulation(genepool) {

    /** Breed the top 5 performers with each other  */
    var new_pop = []
    for (var i = 0; i < 5; i += 1) {
        for (var j = 0; j < 5; j += 1) {
            if (j != i) { 
                new_pop.push(crossover(genepool[i], genepool[j]));
            }
        }
    }

    /** The new pool is the offspring and the best of the last generation  */
    return new_pop.concat(genepool).slice(0, N_NPCS);
}


function crossover(p1, p2) {
    var point = Math.floor(p1.length*Math.random());
    return p1.slice(0,point).concat(p2.slice(point));
}

function preload () {
    this.load.image('arrow','assets/arrow.png');
    this.load.image('wall','assets/wall.png');
    this.load.image('npc','assets/npc.png');
    this.load.image('star','assets/star.png');
}

function hitGoal() {
    pause();
    player.score = levenshtein(player.history, yardstick);
    var genepool_scores = [];
    npcs.children.iterate(function (child) {
        child.score = levenshtein(player.history,child.history);
        genepool_scores.push({gene:child.gene,score:child.score});
    });
    genepool_scores.sort((a, b) => {
        return a.score - b.score;
    });
    console.log("Best bot: " + genepool_scores[0].score);
    if (player.score > genepool_scores[0].score) {
        console.log("Observer correctly detected player");
    }
    else {
        console.log("Observer mistook bot for player");
    }

    /** Create the new population */
    var genepool = [];
    for (let gene_score of genepool_scores) {
        genepool.push(gene_score.gene);
    }
    var newpop = getNewPopulation(genepool);

    /** reset the npc's */
    npcs.children.iterate(function (child) {
        child.score = 0;
        child.gene = newpop.pop();
        child.x = 10;
        child.y = 10;
        child.history = "";
        child.pointer = Math.floor(GENE_LEN*Math.random());
    });

    /** reset the player */
    player.x = 10;
    player.y = 10;
    player.history = "";

    game.scene.resume('default');

}

function create () {
    cursors = this.input.keyboard.createCursorKeys();

    player = this.physics.add.image(100,100,'arrow');

    var goal = this.physics.add.image(750,750,'star');

    npcs = this.physics.add.group({
        key: 'npc',
        repeat: N_NPCS - 1,
        setXY: { x: 10, y:10 }
    });

    /** Generate the maze */
    walls = this.physics.add.staticGroup();
    for (var j = 200; j < 800; j += 200) {
        for (var i = 0 ; i < 700; i += 50){
            walls.create(i, j, 'wall');
        }
        for (var i = 100 ; i < 850; i += 50){
            walls.create(i, j + 100, 'wall');
        }
    }

    player.setCollideWorldBounds(true);
    npcs.children.iterate(function (child) {
        child.setCollideWorldBounds(true);
        child.gene = genRandomGene(GENE_LEN);
        child.pointer = Math.floor(child.gene.length*Math.random());
        child.history = "";
    });
    this.physics.add.collider(player, walls);
    this.physics.add.collider(npcs, player);
    this.physics.add.collider(npcs, walls);
    player.history = "";

    this.physics.add.overlap(player, goal, hitGoal, null, this);
}

function update ()
{
    if (cursors.left.isDown && cursors.up.isDown) {
        player.setVelocityX(-Math.SQRT1_2 * SPEED);
        player.setVelocityY(-Math.SQRT1_2 * SPEED);
        player.angle = -45;
        player.history += 'a';
    }
    else if (cursors.right.isDown && cursors.up.isDown) {
        player.setVelocityX(Math.SQRT1_2 * SPEED);
        player.setVelocityY(-Math.SQRT1_2 * SPEED);
        player.angle = 45;
        player.history += 'b';
    }
    else if (cursors.right.isDown && cursors.down.isDown) {
        player.setVelocityX(Math.SQRT1_2 * SPEED);
        player.setVelocityY(Math.SQRT1_2 * SPEED);
        player.angle = 135;
        player.history += 'c';
    }
    else if (cursors.left.isDown && cursors.down.isDown) {
        player.setVelocityX(-Math.SQRT1_2 * SPEED);
        player.setVelocityY(Math.SQRT1_2 * SPEED);
        player.angle = -135;
        player.history += 'd';
    }
    else if (cursors.left.isDown) {
        player.setVelocityX(-SPEED);
        player.setVelocityY(0);
        player.angle = -90;
        player.history += 'l';
    }
    else if (cursors.right.isDown) {
        player.setVelocityX(SPEED);
        player.setVelocityY(0);
        player.angle = 90;
        player.history += 'r';
    }
    else if (cursors.up.isDown) {
        player.setVelocityY(-SPEED);
        player.setVelocityX(0);
        player.angle = 0;
        player.history += 'u';
    }
    else if (cursors.down.isDown) {
        player.setVelocityY(SPEED);
        player.setVelocityX(0);
        player.angle = 180;
        player.history += 'd';
    } 
    else {
        player.setVelocity(0);
        player.history += ' ';
    }


    /** Update NPC's */
    npcs.children.iterate(function (child) {

        var dir = child.gene[child.pointer];
        switch (dir) {
            case 'r':
                child.setVelocityX(SPEED);
                child.setVelocityY(0);
                child.angle = 90;
                break;
            case 'l':
                child.setVelocityX(-SPEED);
                child.setVelocityY(0);
                child.angle = -90;
                break;
            case 'u':
                child.setVelocityX(0);
                child.setVelocityY(-SPEED);
                child.angle = 0;
                break;
            case 'd':
                child.setVelocityX(0);
                child.setVelocityY(SPEED);
                child.angle = 180;
                break;
            case 'a':
                child.setVelocityX(-Math.SQRT1_2 * SPEED);
                child.setVelocityY(-Math.SQRT1_2 * SPEED);
                child.angle = -45;
                break;
            case 'b':
                child.setVelocityX(Math.SQRT1_2 * SPEED);
                child.setVelocityY(-Math.SQRT1_2 * SPEED);
                child.angle = 45;
                break;
            case 'c':
                child.setVelocityX(Math.SQRT1_2 * SPEED);
                child.setVelocityY(Math.SQRT1_2 * SPEED);
                child.angle = 135;
                break;
            case 'd':
                child.setVelocityX(-Math.SQRT1_2 * SPEED);
                child.setVelocityY(Math.SQRT1_2 * SPEED);
                child.angle = -135;
                break;
            case ' ':
                child.setVelocity(0);
        }
        child.history += dir;
        child.pointer = (child.pointer + 1) % child.gene.length;
    }); 
}

function getHumanScore(history) {
    return levenshtein(history,yardstick);
}

function pause() {
    game.scene.pause('default');
}